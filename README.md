# RedisJoinLeave

RedisJoinLeave is a small utility plugin that broadcasts leave and join messages to all configured Minecraft servers.

## Installation

Simply drop the plugin into all BungeeCord plugin folders and restart.

## Permissions

None.

## License

This plugin is released under AGPL 3.0.
