/*-
 * #%L
 * redisjoinleave
 * %%
 * Copyright (C) 2018 Foorack
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package net.cubekrowd.redisjoinleave.bungeecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class KrowdListener implements Listener {

    private final RedisJoinLeavePlugin plugin;
    // unchecked (possibly banned) users
    private final ArrayList<UUID> status;

    public KrowdListener(RedisJoinLeavePlugin plugin) {
        this.plugin = plugin;
        status = new ArrayList<UUID>();
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        status.add(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onServerConnected(ServerConnectedEvent e) {
        long time = System.currentTimeMillis();

        if(status.contains(e.getPlayer().getUniqueId())) {
            status.remove(e.getPlayer().getUniqueId());

            Collection<ServerInfo> siList;
            if(plugin.getConfig().getStringList("servers").size() == 0) {
                siList = plugin.getProxy().getServers().values();
            } else {
                siList = plugin.getConfig().getStringList("servers").stream().map(s -> plugin.getProxy().getServerInfo(s)).filter(Objects::nonNull).collect(Collectors.toList());
            }
            TranslatableComponent msg = new TranslatableComponent("multiplayer.player.joined");
            msg.setColor(ChatColor.YELLOW);
            msg.addWith(e.getPlayer().getName());
            siList.forEach(si -> si.getPlayers().forEach(pp -> pp.sendMessage(msg)));

            if(plugin.isEventStorageAPI()) {
                Map<String, String> data = new HashMap<>();
                data.put("u", e.getPlayer().getUniqueId().toString()); // uuid
                data.put("v", "" + e.getPlayer().getPendingConnection().getVersion()); // protocol version
                data.put("h", e.getPlayer().getPendingConnection().getVirtualHost().getHostName()); // hostname
                data.put("i", e.getPlayer().getPendingConnection().getAddress().getAddress().getHostAddress()); // ip-address
                data.put("b", "bungee"); // bungee id
                CompletableFuture.runAsync(() -> {
                    EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "login", time, data));
                });

                final String uuid = e.getPlayer().getUniqueId().toString();
                Map<String, String> ftjd = Map.of("u", e.getPlayer().getUniqueId().toString());

                CompletableFuture.runAsync(() -> {
                    long count = EventStorageAPI.getStorage().getEntryStream(plugin.getDescription().getName(), Arrays.asList("first_time"), 0, System.currentTimeMillis(), ftjd).count();
                    if(count == 0) {
                        EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "first_time", time, ftjd));

                        plugin.getConfig().getStringList("first-join-commands").forEach(c -> plugin.getProxy().getPluginManager().dispatchCommand(e.getPlayer(), c));
                    }
                });
            }
        }

        if(plugin.isEventStorageAPI()) {
            Map<String, String> data = new HashMap<>();
            data.put("u", e.getPlayer().getUniqueId().toString());
            data.put("s", e.getServer().getInfo().getName());
            CompletableFuture.runAsync(() -> {
                EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "server_join", time, data));
            });
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        if(status.contains(e.getPlayer().getUniqueId())) {
            status.remove(e.getPlayer().getUniqueId());
            return;
        }

        Collection<ServerInfo> siList;
        if(plugin.getConfig().getStringList("servers").size() == 0) {
            siList = plugin.getProxy().getServers().values();
        } else {
            siList = plugin.getConfig().getStringList("servers").stream().map(s -> plugin.getProxy().getServerInfo(s)).filter(Objects::nonNull).collect(Collectors.toList());
        }
        TranslatableComponent msg = new TranslatableComponent("multiplayer.player.left");
        msg.setColor(ChatColor.YELLOW);
        msg.addWith(e.getPlayer().getName());
        siList.forEach(si -> si.getPlayers().forEach(pp -> pp.sendMessage(msg)));

        if(plugin.isEventStorageAPI()) {
            Map<String, String> data = new HashMap<>();
            data.put("u", e.getPlayer().getUniqueId().toString());
            CompletableFuture.runAsync(() -> {
                EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "logout", data));
            });
        }
    }
}
