/*-
 * #%L
 * redisjoinleave
 * %%
 * Copyright (C) 2018 Foorack
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package net.cubekrowd.redisjoinleave.bungeecord;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.logging.Level;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class RedisJoinLeavePlugin extends Plugin {
    @Getter private boolean eventStorageAPI = false;
    @Getter private Configuration config;

    @Override
    public void onEnable() {
        if (getProxy().getPluginManager().getPlugin("EventStorageAPI") == null) {
            getLogger().info("EventStorageAPI not detected. Skipping logging...");
            eventStorageAPI = false;
        } else {
            getLogger().info("EventStorageAPI detected. Using it for logging.");
            eventStorageAPI = true;
        }

        saveDefaultConfig("config.yml");
        config = loadConfig("config.yml");

        getProxy().getPluginManager().registerListener(this, new KrowdListener(this));
        getProxy().getPluginManager().registerCommand(this, new FirstTimeJoinCommand(this));
    }

    @Override
    public void onDisable() {
        config = null;
        eventStorageAPI = false;
    }

    public void saveDefaultConfig(String name) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), name);
        try {
            Files.copy(getResourceAsStream(name), configFile.toPath());
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save default config " + name, e);
        }
    }

    public Configuration loadConfig(String name) {
        var configProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        try {
            return configProvider.load(new File(getDataFolder(), name));
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't load config " + name, e);
            return new Configuration();
        }
    }
}
