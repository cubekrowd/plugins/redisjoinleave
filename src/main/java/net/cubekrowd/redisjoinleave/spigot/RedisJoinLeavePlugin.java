/*-
 * #%L
 * redisjoinleave
 * %%
 * Copyright (C) 2018 Foorack, lekro
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package net.cubekrowd.redisjoinleave.spigot;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.*;

public class RedisJoinLeavePlugin extends JavaPlugin implements Listener {

    @Getter private boolean eventStorageAPI;

    @Override
    public void onEnable() {

        if (getServer().getPluginManager().getPlugin("EventStorageAPI") == null) {
            getLogger().info("EventStorageAPI not detected. Skipping logging...");
            eventStorageAPI = false;
        } else {
            getLogger().info("EventStorageAPI detected. Using it for logging.");
            eventStorageAPI = true;
        }

        getCommand("firstjoin").setExecutor(new CommandHandler(this));

    }

    @Override
    public void onDisable() {
        eventStorageAPI = false;
    }

}
