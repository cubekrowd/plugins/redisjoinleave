#!/bin/bash

set -x

# CUBEKROWD LICENSEING SCRIPT

# THIS SCRIPT AUTOMATICALLY SETS THE LICENSE HEADERS OF ALL FILES IN A PROJECT TO THE GIT AUTHORS OF THAT FILE

# THIS SCRIPT SHOULD BE RUN IN THE PROJECT FOLDER

if [ ! -f pom.xml ]; then
	echo "Invalid syntax! Please run this script in the project folder!"
	exit 1
fi

# inceptionYear and organizationName is required but not used.
mvn license:update-project-license -Dlicense.inceptionYear=0 -Dlicense.organizationName="" -Dlicense.licenseName=agpl_v3

for f in $(find src -name '*.java' -exec echo {} \;); do
	echo $f
	AUTHORS=$(git log $f | grep Author | cut -d ' ' -f 2 | grep -vi cubekrowd | sort | uniq | paste -sd "," - | sed "s/,/, /g")
	FILEPATH=$(echo $f | cut -d "/" -f 4-)
	mvn license:update-file-header -Dlicense.inceptionYear=2018 -Dlicense.organizationName="$AUTHORS" -Dlicense.licenseName=agpl_v3 -Dlicense.emptyLineAfterHeader=true -Dlicense.addJavaLicenseAfterPackage=false -Dlicense.canUpdateCopyright=true -Dlicense.includes="**/$FILEPATH" -X
done
