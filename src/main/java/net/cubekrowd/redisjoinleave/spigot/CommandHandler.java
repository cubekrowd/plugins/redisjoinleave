/*-
 * #%L
 * redisjoinleave
 * %%
 * Copyright (C) 2018 Foorack, lekro
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package net.cubekrowd.redisjoinleave.spigot;

import java.io.File;
import java.util.*;

import org.bukkit.*;
import org.bukkit.command.*;

import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;

import lombok.*;

@RequiredArgsConstructor
public class CommandHandler implements CommandExecutor {

    private final RedisJoinLeavePlugin plugin;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length != 1 || !args[0].equals("convert")) return false;

        if (!plugin.isEventStorageAPI()) {
            sender.sendMessage(ChatColor.RED + "Error: Could not load ESAPI.");
            return true;
        }

        sender.sendMessage(ChatColor.RED + "WARNING. Pushing ALL first join data to ESAPI.");

        // Get directory of main world
        File playerData = new File(Bukkit.getWorlds().get(0).getWorldFolder(), "playerdata");
        File[] files = playerData.listFiles();
        for(int i = 0; i != files.length; i++) {
            plugin.getLogger().info("Converting " + files[i].getName() + " [" + (i + 1) + "/" + files.length + "]");
            // Get OfflinePlayer
            // @NOTE(traks) file can also end with .dat_old
            UUID uuid = UUID.fromString(files[i].getName().replaceAll("\\.dat.*", ""));
            OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);

            // Create event
            EventStorageAPI.getStorage().addEntry(new EventEntry("RedisJoinLeave", "first_time", player.getFirstPlayed(), Map.of("u", uuid.toString())));
        }

        sender.sendMessage(ChatColor.GREEN + "First-time login times successfully pushed to ESAPI.");
        return true;
    }

}
