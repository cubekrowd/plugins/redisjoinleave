/*-
 * #%L
 * redisjoinleave
 * %%
 * Copyright (C) 2018
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package net.cubekrowd.redisjoinleave.bungeecord;

import java.time.*;
import java.util.*;
import net.md_5.bungee.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.api.config.*;
import net.md_5.bungee.api.plugin.*;
import net.cubekrowd.eventstorageapi.api.*;
import lombok.*;

public class FirstTimeJoinCommand extends Command {

    private final RedisJoinLeavePlugin plugin;

    public FirstTimeJoinCommand(RedisJoinLeavePlugin plugin) {
        super("firsttimejoin", null, "firsttime");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(!plugin.isEventStorageAPI()) {
            sender.sendMessage(new TextComponent("Logging is not enabled. Please install EventStorageAPI."));
            return;
        }
        long timestamp;
        if(args.length == 0) {
            if(!sender.hasPermission("redisjoinleave.firsttimejoin.self")) {
                sender.sendMessage(new TextComponent(ChatColor.RED + "No permission."));
                return;
            }
            if(!(sender instanceof ProxiedPlayer)) {
                sender.sendMessage(new TextComponent(ChatColor.RED + "Usage: /firsttimejoin <uuid>"));
                return;
            }
            timestamp = getTime(((ProxiedPlayer) sender).getUniqueId().toString());
        } else {
            if(!sender.hasPermission("redisjoinleave.firsttimejoin.others")) {
                sender.sendMessage(new TextComponent(ChatColor.RED + "No permission."));
                return;
            }
            timestamp = getTime(args[0].replaceAll("[^a-zA-Z0-9\\-]", ""));
        }

        if(timestamp == -1) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "Player has never been online. Did you enter their UUID?"));
            return;
        }

        Instant in = Instant.ofEpochMilli(timestamp);
        sender.sendMessage(new TextComponent(ChatColor.AQUA + "First time join: " + in.toString().replace('T', ' ').replace('Z', ' ')));
    }

    private long getTime(String uuid) {
        List<EventEntry> list = EventStorageAPI.getStorage().getEntryList("redisjoinleave", Arrays.asList("first_time"), 0, System.currentTimeMillis(), Map.of("u", uuid.toLowerCase()));
        if(list.size() == 0) {
            return -1;
        } else {
            return list.get(0).getTime();
        }
    }

}
